library(twitteR)
library(stringr)

api_key <- 'bmkYH6wJBXqiDbFCCfmiCaQ0q'
api_secret <- 'YdburLdYqM4uFyWy3aqDDLxiPMrjU3URgiFbO2ZNLJE6uE7XVp'
access_token <- '1443987297133805572-PkM0g3T21gDrUpuIsFVbw7yUM5G44m'
access_token_secret <- 'gzyc8FqwAov06mwBFhQgi0wU45kh2jq1MJa4qkXcv4Ypp'

conjunto1 <- function() {
  setup_twitter_oauth(api_key, api_secret, access_token, access_token_secret)
  tweets <- searchTwitter('virgen', n=10, lang='es')

  listaWords <- c()
  
  tweets_df = twListToDF(tweets)
  for (tW in tweets_df['text']) {
    for (Words in strsplit(clean_text(tW), " ")) {
        for (word in Words) {
          if (nchar(word) > 2){
            word <- tolower(word)
            if(words_no_important(word = word)){
              listaWords <- c(listaWords, word)
            }
          }
        }
    }
  }
  
  mapaConjuntos <- c()
  for (l in listaWords) {
    mapaConjuntos[l] <- 0
  }
  mapaFinal <- c()
  repetido <- FALSE
  
  for (m in seq(1,length(mapaConjuntos))) {
    palabra <- mapaConjuntos[m]
    repetido <- FALSE
#    print(paste(names(palabra), "goes", palabra))
    
    for (valor in names(mapaFinal)) {
      if(names(palabra) == valor){
        mapaFinal[names(palabra)] <- 1
        repetido = TRUE
        break
      }
    }
    
    if(repetido == FALSE){
      mapaFinal[names(palabra)] <- mapaFinal[names(palabra)] + 1
    }
  }
  
  for (m in seq(1,length(mapaFinal))) {
    palabra <- mapaFinal[m]
    print(paste(names(palabra), "goes", palabra))
  }

  return(tweets)
}

words_no_important <- function(word){
  wordsNoImportant <- c("las", "muy", "les", "los", "como", "del", "por", "ende", "ella", "entre", "mientras", "hace", "entonces", "para", "que", "cuando", "porque", "para", "pero", "ven", "nosotros", "ellos", "sin", "donde", "con", "vas", "creo", "más", "este", "sobre", "tras", "sus", "una", "desde", "fue", "qué", "están", "esta", "esto", "está", "ser", "tiene", "hasta", "eso", "son", "mis", "nos", "tal", "hay", "allá", "otro", "soy", "vos", "sea", "osea", "van")
  palabraValida <- TRUE
  
  for (w in wordsNoImportant) {
    if(word == w){
      palabraValida <- FALSE
      break
    }
  }
  
  return(palabraValida)
}

clean_text <- function(unclean_tweet){
  clean_tweet = gsub("&amp", "", unclean_tweet)
  clean_tweet = gsub("(RT|via)((?:\\b\\W*@\\w+)+)", "", clean_tweet)
  clean_tweet = gsub("@\\w+", "", clean_tweet)
  clean_tweet = gsub("[[:punct:]]", "", clean_tweet)
  clean_tweet = gsub("[[:digit:]]", "", clean_tweet)
  clean_tweet = gsub("http\\w+", "", clean_tweet)
  clean_tweet = gsub("[ \t]{2,}", "", clean_tweet)
  clean_tweet = gsub("^\\s+|\\s+$", "", clean_tweet)

  #get rid of unnecessary spaces
  clean_tweet <- str_replace_all(clean_tweet," "," ")
  # Get rid of URLs
  # Take out retweet header, there is only one
  clean_tweet <- str_replace(clean_tweet,"RT @[a-z,A-Z]*: ","")
  # Get rid of hashtags
  clean_tweet <- str_replace_all(clean_tweet,"#[a-z,A-Z]*","")
  # Get rid of references to other screennames
  clean_tweet <- str_replace_all(clean_tweet,"@[a-z,A-Z]*","")
  
  stopwords = c("a", "able", "about", "above", "abst", "accordance", "yourself",
                "yourselves", "you've", "z", "zero")
  
  return(clean_tweet)
}

opcion <- "-1"
boolOp1 <- FALSE
boolOp2 <- FALSE

while (opcion !="0"){
  print("----------MENU-----------")
  print("|1:    Conjunto 1       |")
  print("-------------------------")
  print("|2:    Conjunto 2       |")
  print("-------------------------")
  if (boolOp1 == TRUE & boolOp2 == TRUE){
    print("|3:       Union         |")
    print("-------------------------")
    print("|4:   Interseccion      |")
    print("-------------------------")
  }
  
  print("|5: Grafico por fecha   |")
  print("-------------------------")
  print("|6: Graf. n. de personas|")
  print("-------------------------")
  print("|7:  Graf. n. de tweets |")
  print("-------------------------")
  if (boolOp1 == TRUE & boolOp2 == TRUE){
    print("|8: Graf. palabras dif. |")
    print("-------------------------")
  }
  print("|0:      Salir          |")
  print("-------------------------")
  print("Seleccione la opcion: ")
  opcion <- readline()

  if(opcion == 1){
    boolOp1 <- TRUE
    conjunto1()
#    mapaConjuntos.put("Conjunto1", conjunto1());
  }else if(opcion == "2"){
    boolOp2 <- TRUE
#    mapaConjuntos.put("Conjunto2", conjunto2());
  }else if(opcion == "3" & boolOp1 == TRUE & boolOp2 == TRUE){
#    union(mapaConjuntos);
  }else if(opcion == "4" & boolOp1 == TRUE & boolOp2 == TRUE){
#    interseccion(mapaConjuntos);
  }else if(opcion == "5"){
#    graficoNumeroDeTweetsPorFecha();
  }else if(opcion == "6"){
#    graficoNumeroDePersonasPorFecha();
  }else if(opcion == "7"){
#    graficoNumeroDeTweetsPorUsuario();
  }else if(opcion == "8" & boolOp1 == TRUE & boolOp2 == TRUE){
#    graficoPalabrasDistintasEnConjuntos(mapaConjuntos);
  }else if(opcion == "0"){
    break
  }else{
    print("Opcion incorrecta. Vuelva a intentar");
  }
}