require 'twitter'
require 'remove_emoji'

$client = Twitter::REST::Client.new do |config|
  config.consumer_key        = "bmkYH6wJBXqiDbFCCfmiCaQ0q"
  config.consumer_secret     = "YdburLdYqM4uFyWy3aqDDLxiPMrjU3URgiFbO2ZNLJE6uE7XVp"
  config.access_token        = "1443987297133805572-PkM0g3T21gDrUpuIsFVbw7yUM5G44m"
  config.access_token_secret = "gzyc8FqwAov06mwBFhQgi0wU45kh2jq1MJa4qkXcv4Ypp"
end

# puts gets

# tweets.each { |tweet| puts tweet.full_text }

boolOp1 = false
boolOp2 = false
option = '-1'
hashes = {}

#paises.each do |clave, valor|
#    puts "#{clave} #{valor}"
#end

def cleanText(texto)
    texto = texto.downcase

    texto = texto.gsub(/https?:\/\/.\S+/, '')
    texto = texto.gsub(/^RT[\s]+/, '')

    texto = RemoveEmoji::Sanitize.call(texto)

    texto = texto.gsub(/[!@#$%^&*…()-=_¡+|;':"“,.<>?”']/, '')
    texto = texto.gsub(/\n/, '')
    texto = texto.gsub(/\t/, '')
    texto = texto.gsub(/\d/, '')

    #remove some unnecessary words
    texto = texto.gsub('las', '')
    texto = texto.gsub('les', '')
    texto = texto.gsub('los', '')
    texto = texto.gsub('como', '')
    texto = texto.gsub('del', '')
    texto = texto.gsub('por', '')
    texto = texto.gsub('ende', '')
    texto = texto.gsub('ella', '')
    texto = texto.gsub('entre', '')
    texto = texto.gsub('mientras', '')
    texto = texto.gsub('hace', '')
    texto = texto.gsub('entonces', '')
    texto = texto.gsub('para', '')
    texto = texto.gsub('que', '')
    texto = texto.gsub('cuando', '')
    texto = texto.gsub('porque', '')
    texto = texto.gsub('para', '')
    texto = texto.gsub('pero', '')
    texto = texto.gsub('ven', '')
    texto = texto.gsub('nosotros', '')
    texto = texto.gsub('ellos', '')
    texto = texto.gsub('sin', '')
    texto = texto.gsub('donde', '')
    texto = texto.gsub('con', '')
    texto = texto.gsub('vas', '')
    texto = texto.gsub('creo', '')
    texto = texto.gsub('más', '')
    texto = texto.gsub('este', '')
    texto = texto.gsub('sobre', '')
    texto = texto.gsub('tras', '')
    texto = texto.gsub('sus', '')
    texto = texto.gsub('una', '')
    texto = texto.gsub('desde', '')
    texto = texto.gsub('fue', '')
    texto = texto.gsub('qué', '')
    texto = texto.gsub('están', '')
    texto = texto.gsub('esta', '')
    texto = texto.gsub('esto', '')
    texto = texto.gsub('está', '')
    texto = texto.gsub('ser', '')
    texto = texto.gsub('tiene', '')
    texto = texto.gsub('hasta', '')
    texto = texto.gsub('eso', '')
    texto = texto.gsub('son', '')
    texto = texto.gsub('mis', '')
    texto = texto.gsub('nos', '')
    texto = texto.gsub('tal', '')
    texto = texto.gsub('hay', '')
    texto = texto.gsub('allá', '')
    texto = texto.gsub('otro', '')
    texto = texto.gsub('soy', '')
    texto = texto.gsub('vos', '')
    texto = texto.gsub('sea', '')
    texto = texto.gsub('osea', '')
    texto = texto.gsub('van', '')
end


def conjunto1
    listWordsFinal = []
    tweets = $client.user_timeline('adidas', count: 20)

    tweets.each do |tw|
        listWords = cleanText(tw.full_text)
        listWords = listWords.split
        listWords.each do |lw|
            if lw.length > 2
                listWordsFinal << lw
            end
        end
    end

    dictionnaryWords = {}
    listWordsFinal.each do |lw|
        if dictionnaryWords.has_key?(lw)
            dictionnaryWords[lw] = dictionnaryWords[lw] + 1
        else
            dictionnaryWords[lw] = 1
        end
    end

    thirtyCount = 30
    mayor = 0
    palabraMayor = ''
    dictWordsMostRepeat = {}

    while thirtyCount > 0  
        dictionnaryWords.each do |clave, valor| 
            if dictionnaryWords[clave] > mayor
                mayor = dictionnaryWords[clave]
                palabraMayor = clave
            end
        end

        dictWordsMostRepeat[palabraMayor] = dictionnaryWords[palabraMayor]
        dictionnaryWords[palabraMayor] = 0
        mayor = 0

        thirtyCount = thirtyCount - 1
    end

    listWordsX = []
    listWordsY = []

    dictWordsMostRepeat.each do |clave, valor| 
        listWordsX << clave
        listWordsY << valor
    end

    crear_histograma(listWordsX, listWordsY)

end


def crear_histograma(abcisas, ordenadas)
    contador = 0

    ordenadas.each do |o|
        print "#{abcisas[contador]} : "
        contador = contador + 1
        o.times do |abcisas, ordenadas|
            print '*'
        end
        puts ' '
    end


end

# ''

def conjunto2
    listWordsFinal2 = []
    tweets = $client.user_timeline('nike', count: 20)

    tweets.each do |tw|
        listWords2 = cleanText(tw.full_text)
        listWords2 = listWords2.split
        listWords2.each do |lw|
            if lw.length > 2
                listWordsFinal2 << lw
            end
        end
    end

    dictionnaryWords2 = {}
    listWordsFinal2.each do |lw|
        if dictionnaryWords2.has_key?(lw)
            dictionnaryWords2[lw] = dictionnaryWords2[lw] + 1
        else
            dictionnaryWords2[lw] = 1
        end
    end

    thirtyCount2 = 30
    mayor2 = 0
    palabraMayor2 = ''
    dictWordsMostRepeat2 = {}

    while thirtyCount2 > 0  
        dictionnaryWords2.each do |clave, valor| 
            if dictionnaryWords2[clave] > mayor2
                mayor2 = dictionnaryWords2[clave]
                palabraMayor2 = clave
            end
        end

        dictWordsMostRepeat2[palabraMayor2] = dictionnaryWords2[palabraMayor2]
        dictionnaryWords2[palabraMayor2] = 0
        mayor2 = 0

        thirtyCount2 = thirtyCount2 - 1
    end


    listWordsX2 = []
    listWordsY2 = []

    dictWordsMostRepeat2.each do |clave, valor| 
        listWordsX2 << clave
        listWordsY2 << valor
    end

    crear_histograma(listWordsX2, listWordsY2)
end


def union(mapaConjuntos, palabrasComunes)

    dictWordsMostRepeatUnion = {}

    mapaConjuntos["Conjunto1"].each do |clave1, valor1| 
        mapaConjuntos["Conjunto2"].each do |clave2, valor2|
            if clave1 == clave2
                dictWordsMostRepeatUnion[clave1] = mapaConjuntos["Conjunto1"][clave1] + mapaConjuntos["Conjunto2"][clave2]
                palabrasComunes << clave1
                break
            end
        end
    end

    mapaConjuntos["Conjunto1"].each do |clave1, valor1| 
        if !(palabrasComunes.include?(clave1))
            dictWordsMostRepeatUnion[clave1] = mapaConjuntos["Conjunto1"][clave1]
        end
    end

    mapaConjuntos["Conjunto2"].each do |clave2, valor2| 
        if !(palabrasComunes.include?(clave2))
            dictWordsMostRepeatUnion[clave2] = mapaConjuntos["Conjunto2"][clave2]
        end
    end

    dictWordsMostRepeatUnion.each do |clavef, valorf|
        puts "#{clavef}"
        puts valorf
    end
end


def interseccion(mapaConjuntosI)
    dictInterseccion = {}

    cantidad = 0
    mapaConjuntosI["Conjunto1"].each do |clave1, valor1| 
        mapaConjuntosI["Conjunto2"].each do |clave2, valor2|
            if clave1 == clave2
                dictInterseccion[clave1] = mapaConjuntosI["Conjunto1"][clave1] + mapaConjuntosI["Conjunto2"][clave2]
                cantidad = cantidad + 1
                break
            end
        end
        if cantidad == 30
            break
        end
    end

    thirtyCountInterseccion = dictInterseccion.length
    mayorInterseccion = 0
    palabraMayorInterseccion = ''
    dictWordsMostRepeatInterseccion = {}

    while thirtyCountInterseccion > 0  
        dictInterseccion.each do |clave, valor| 
            if dictInterseccion[clave] > mayorInterseccion
                mayorInterseccion = dictInterseccion[clave]
                palabraMayorInterseccion = clave
            end
        end

        dictWordsMostRepeatInterseccion[palabraMayorInterseccion] = dictInterseccion[palabraMayorInterseccion]
        dictInterseccion[palabraMayorInterseccion] = 0
        mayorInterseccion = 0

        thirtyCountInterseccion = thirtyCountInterseccion - 1
    end

    dictWordsMostRepeatInterseccion.each do |clavef, valorf|
        puts "#{clavef}"
        puts valorf
    end
end


def graficoNumeroDeTweetsPorFecha
    tweets5 = $client.search("#sopa", { tweet_mode: "extended", include_entities: true, fromDate:"202001071200",toDate:"202002071200", entities: true, count: 1})
    fecha = ""
    cantidad = 1
    dictFechas = {}

    tweets5.each do |tw|
        if tw.created_at.strftime("%d/%m/%Y") != fecha
            fecha = tw.created_at.strftime("%d/%m/%Y")
            cantidad = 1
        end

        dictFechas[fecha] = cantidad
        cantidad = cantidad + 1
    end

    dictFechas.each do |clavef, valorf|
        puts "#{clavef}"
        puts valorf
#        puts tw.user.screen_name
    end

    listFechasG5 = []
    listTweetsG5 = []

    dictFechas.each do |clave, valor| 
        listFechasG5 << clave
        listTweetsG5 << valor
    end

    crear_histograma(listFechasG5, listTweetsG5)

end


def graficoNumeroDePersonasPorFecha
    tweets6 = $client.search("#sopa", { tweet_mode: "extended", include_entities: true, fromDate:"202001071200",toDate:"202002071200", entities: true, count: 1})
    fecha = ""
    name = ""
    mismoUsuario = false
    dictFechasNombres = {}
    listOfDifferentUsers = []
    cantidad = 1

    tweets6.each do |tw|
        mismoUsuario = false
        if tw.created_at.strftime("%d/%m/%Y") != fecha
            fecha = tw.created_at.strftime("%d/%m/%Y")
            listOfDifferentUsers = []
            cantidad = 1
        end

        listOfDifferentUsers.each do |user|
            if user == tw.user.screen_name
                mismoUsuario = true
                break
            end
        end

        dictFechasNombres[fecha] = cantidad
        cantidad = cantidad + 1

        if mismoUsuario == false
            listOfDifferentUsers << tw.user.screen_name
            dictFechasNombres[fecha] = cantidad
            cantidad = cantidad + 1
        end
    end

    dictFechasNombres.each do |clavef, valorf|
        puts "#{clavef}"
        puts valorf
    end


    listFechasG6 = []
    listPersonasG6 = []

    dictFechasNombres.each do |clave, valor| 
        listFechasG6 << clave
        listPersonasG6 << valor
    end

    crear_histograma(listFechasG6, listPersonasG6)

end


def graficoNumeroDeTweetsPorUsuario
    tweets7 = $client.search("#paraguay", { tweet_mode: "extended", include_entities: true, fromDate:"202001071200",toDate:"202002071200", entities: true, count: 1})
    existe = false
    dictUsuarios = {}

    tweets7.each do |tw|
        existe = false
        dictUsuarios.each do |tw2|
            if tw.user.screen_name == tw2
                existe = True
                break
            end
        end

        if existe == false
            dictUsuarios[tw.user.screen_name] =  1
        else
            dictUsuarios[tw.user.screen_name] = dictUsuarios[tw.user.screen_name] + 1
        end
    end

    dictUsuarios.each do |clavef, valorf|
        puts "#{clavef}"
        puts valorf
    end


    listUsuariosG7 = []
    listCantidadG7 = []

    dictUsuarios.each do |clave, valor| 
        listUsuariosG7 << clave
        listCantidadG7 << valor
    end

    crear_histograma(listUsuariosG7, listUsuariosG7)

end


def graficoPalabrasDistintasEnConjuntos(mapaConjuntos, palabrasComunes)
    wordsDifferentBetweenGroupsList = []

    mapaConjuntos["Conjunto1"].each do |clave1, valor1|
        if !(palabrasComunes.include?(clave1))
            wordsDifferentBetweenGroupsList << clave1
        end
    end

    mapaConjuntos["Conjunto2"].each do |clave2, valor2|
        if !(palabrasComunes.include?(clave2))
            wordsDifferentBetweenGroupsList << clave2
        end
    end

    puts "Las Palabras que difieren de un conjunto y otro son: "
    wordsDifferentBetweenGroupsList.each do |clavef, valorf|
        puts "#{clavef}"
        puts valorf
    end

end

mapaConjuntos = {}
palabrasComunes = []

while option != '0'
    puts "----------MENU-----------"
    puts "|1:    Conjunto 1       |"
    puts "-------------------------"
    puts "|2:    Conjunto 2       |"
    puts "-------------------------"
    if boolOp1 and boolOp2
        puts "|3:       Union         |"
        puts "-------------------------"
        puts "|4:   Interseccion      |"
        puts "-------------------------"
    end
    puts "|5: Grafico por fecha   |"
    puts "-------------------------"
    puts "|6: Graf. n. de personas|"
    puts "-------------------------"  
    puts "|7:  Graf. n. de tweets |"
    puts "-------------------------"
    if boolOp1 and boolOp2
        puts "|8: Graf. palabras dif. |"
        puts "-------------------------"
    end
    puts "|0:      Salir          |"
    puts "-------------------------"

    puts "Seleccione la opcion: "
    option = gets.chomp


    if option == '1'
        boolOp1 = true
        mapaConjuntos["Conjunto1"] = conjunto1
    elsif option == '2'
        boolOp2 = true
        mapaConjuntos["Conjunto2"] = conjunto2
    elsif option == '3' and boolOp1 and boolOp2
        union(mapaConjuntos, palabrasComunes)
    elsif option == '4' and boolOp1 and boolOp2
        interseccion(mapaConjuntos)
    elsif option == '5'
        graficoNumeroDeTweetsPorFecha
    elsif option == '6'
        graficoNumeroDePersonasPorFecha
    elsif option == '7'
        graficoNumeroDeTweetsPorUsuario
    elsif option == '8' and boolOp1 and boolOp2
        graficoPalabrasDistintasEnConjuntos(mapaConjuntos, palabrasComunes)
    elsif option == '0'
        break
    else
        puts "Opcion incorrecta. Vuelva a intentar"
    end
end

