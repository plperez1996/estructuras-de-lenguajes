from re import search
import tweepy
import webbrowser
import time
import json
import datetime
from collections import Counter
import matplotlib
import nltk
import wordcloud
from wordcloud import WordCloud,STOPWORDS
nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer
import matplotlib.pyplot as plt
import numpy as np
import re

access_token = "1443987297133805572-PkM0g3T21gDrUpuIsFVbw7yUM5G44m"
access_token_secret = "gzyc8FqwAov06mwBFhQgi0wU45kh2jq1MJa4qkXcv4Ypp"

consumer_key = "bmkYH6wJBXqiDbFCCfmiCaQ0q"
consumer_secret = "YdburLdYqM4uFyWy3aqDDLxiPMrjU3URgiFbO2ZNLJE6uE7XVp"

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth, wait_on_rate_limit=True)

def cleaner(tweet):
    tweet = tweet.lower() # convert text to lower-case

    # remove hyperlinks
    tweet = re.sub(r'https?:\/\/.\S+', "", tweet)
    
    # remove hashtags
    # only removing the hash # sign from the word
    tweet = re.sub(r'#', '', tweet)
    
    # remove old style retweet text "RT"
    tweet = re.sub(r'^RT[\s]+', '', tweet)

    #remove punctuations
    punctuations = '''!()-![]{};:+'"\,<>./¨´`“?@#$%^&*_~'''
    tweet = ''.join([i for i in tweet if not i in punctuations])

    #remove emojis
    tweet = re.sub("[^\x00-\x7F]+", "", tweet)

    #remove space, tab and enter
    tweet = re.sub(r"\n", "", tweet)
    tweet = re.sub(r"\d", "", tweet)
    tweet = re.sub(r"\t", "", tweet)

    #remove some unnecessary words
    tweet = re.sub("las", "", tweet)
    tweet = re.sub("les", "", tweet)
    tweet = re.sub("los", "", tweet)
    tweet = re.sub("como", "", tweet)
    tweet = re.sub("del", "", tweet)
    tweet = re.sub("por", "", tweet)
    tweet = re.sub("ende", "", tweet)
    tweet = re.sub("ella", "", tweet)
    tweet = re.sub("entre", "", tweet)
    tweet = re.sub("mientras", "", tweet)
    tweet = re.sub("hace", "", tweet)
    tweet = re.sub("entonces", "", tweet)
    tweet = re.sub("para", "", tweet)
    tweet = re.sub("que", "", tweet)
    tweet = re.sub("cuando", "", tweet)
    tweet = re.sub("porque", "", tweet)
    tweet = re.sub("para", "", tweet)
    tweet = re.sub("pero", "", tweet)
    tweet = re.sub("ven", "", tweet)
    tweet = re.sub("nosotros", "", tweet)
    tweet = re.sub("ellos", "", tweet)
    tweet = re.sub("sin", "", tweet)
    tweet = re.sub("donde", "", tweet)
    tweet = re.sub("con", "", tweet)
    tweet = re.sub("vas", "", tweet)
    tweet = re.sub("creo", "", tweet)
    tweet = re.sub("más", "", tweet)
    tweet = re.sub("este", "", tweet)
    tweet = re.sub("sobre", "", tweet)
    tweet = re.sub("tras", "", tweet)
    tweet = re.sub("sus", "", tweet)
    tweet = re.sub("una", "", tweet)
    tweet = re.sub("desde", "", tweet)
    tweet = re.sub("fue", "", tweet)
    tweet = re.sub("qué", "", tweet)
    tweet = re.sub("están", "", tweet)
    tweet = re.sub("esta", "", tweet)
    tweet = re.sub("esto", "", tweet)
    tweet = re.sub("está", "", tweet)
    tweet = re.sub("ser", "", tweet)
    tweet = re.sub("tiene", "", tweet)
    tweet = re.sub("hasta", "", tweet)
    tweet = re.sub("eso", "", tweet)
    tweet = re.sub("son", "", tweet)
    tweet = re.sub("mis", "", tweet)
    tweet = re.sub("nos", "", tweet)
    tweet = re.sub("tal", "", tweet)
    tweet = re.sub("hay", "", tweet)
    tweet = re.sub("allá", "", tweet)
    tweet = re.sub("otro", "", tweet)
    tweet = re.sub("soy", "", tweet)
    tweet = re.sub("vos", "", tweet)
    tweet = re.sub("sea", "", tweet)
    tweet = re.sub("osea", "", tweet)
    tweet = re.sub("van", "", tweet)

    return tweet

def conjunto1():
    listWords = []

    today = datetime.datetime.today().strftime("%Y-%m-%d")
    last_week = (datetime.datetime.today() - datetime.timedelta(days=7)).strftime("%Y-%m-%d")
    conjunto1 = tweepy.Cursor(api.search_tweets, q= "* since:"+ last_week +" until:"+ today, lang= "es", tweet_mode="extended").items(200)

    tweet_list = []
    for tweet in conjunto1:
        tw = cleaner(tweet.full_text)
        for word in tw.split(" "):
            tweet_list.append(word)
        
    for element in tweet_list:
        if(len(element) > 2):
            listWords.append(element)

    dictionnaryWords = {}
    for i in listWords:
        if i not in dictionnaryWords:
            dictionnaryWords[i] = 1
        else:
            dictionnaryWords[i] += 1

    thirtyCount = 30
    mayor = 0
    palabraMayor = ""
    dictWordsMostRepeat = {}
    while thirtyCount > 0:
        for i in dictionnaryWords:
            if dictionnaryWords[i] > mayor:
                mayor = dictionnaryWords[i]
                palabraMayor = i
        
        dictWordsMostRepeat[palabraMayor] = dictionnaryWords[palabraMayor]
        dictionnaryWords[palabraMayor] = 0
        mayor = 0
        thirtyCount-=1

    palabras = []
    cantidad = []
    for i in dictWordsMostRepeat:
        palabras.append(i)
        cantidad.append(dictWordsMostRepeat[i])

    # GRAFICA
    fig, ax = plt.subplots()
    ax.barh(palabras, cantidad)
    plt.show()

    return dictWordsMostRepeat



def conjunto2():
    listWords2 = []
    
    today = datetime.datetime.today().strftime("%Y-%m-%d")
    last_week = (datetime.datetime.today() - datetime.timedelta(days=7)).strftime("%Y-%m-%d")
    conjunto2 = tweepy.Cursor(api.search_tweets, q="madre"+" since:"+ last_week +" until:"+ today).items(200)

    tweet_list2 = []
    for tweet in conjunto2:
        tw = cleaner(tweet.text)
        for word in tw.split(" "):
            tweet_list2.append(word)
    
            for element in tweet_list2:
                if(len(element) > 2):
                    listWords2.append(element)

    dictionnaryWords2 = {}
    for i in listWords2:
        if i not in dictionnaryWords2:
            dictionnaryWords2[i] = 1
        else:
            dictionnaryWords2[i] += 1

    thirtyCount2 = 30
    mayor2 = 0
    palabraMayor2 = ""
    dictWordsMostRepeat2 = {}
    while thirtyCount2 > 0:
        for i in dictionnaryWords2:
            if dictionnaryWords2[i] > mayor2:
                mayor2 = dictionnaryWords2[i]
                palabraMayor2 = i
        
        dictWordsMostRepeat2[palabraMayor2] = dictionnaryWords2[palabraMayor2]
        dictionnaryWords2[palabraMayor2] = 0
        mayor2 = 0
        thirtyCount2 -= 1

    palabras2 = []
    cantidad2 = []
    for i in dictWordsMostRepeat2:
        palabras2.append(i)
        cantidad2.append(dictWordsMostRepeat2[i])

    # GRAFICA
    fig, ax = plt.subplots()
    ax.barh(palabras2, cantidad2)
    plt.show()

    return dictWordsMostRepeat2
        

option = -1
boolOp1 = False
boolOp2 = False

def union(mapaConjuntos: dict):

    dictWordsMostRepeatUnion = {}

    for x in mapaConjuntos["Conjunto1"]:
        for y in mapaConjuntos["Conjunto2"]:
            if(x == y):
                dictWordsMostRepeatUnion[x] = mapaConjuntos["Conjunto1"][x] + mapaConjuntos["Conjunto2"][y]
                mapaConjuntos["Conjunto1"][x] = 0
                mapaConjuntos["Conjunto2"][y] = 0
                break


    for a in mapaConjuntos["Conjunto1"]:
        if mapaConjuntos["Conjunto1"][a] != 0:
            dictWordsMostRepeatUnion[a] = mapaConjuntos["Conjunto1"][a]

    for b in mapaConjuntos["Conjunto2"]:
        if mapaConjuntos["Conjunto2"][b] != 0:
            dictWordsMostRepeatUnion[b] = mapaConjuntos["Conjunto2"][b]

    
    thirtyCountUnion = 30
    mayorUnion = 0
    palabraMayorUnion = ""
    dictWordsMostRepeatUnionFinal = {}

    while thirtyCountUnion > 0:
        for c in dictWordsMostRepeatUnion:
            if dictWordsMostRepeatUnion[c] > mayorUnion:
                mayorUnion = dictWordsMostRepeatUnion[c]
                palabraMayorUnion = c
        
        dictWordsMostRepeatUnionFinal[palabraMayorUnion] = dictWordsMostRepeatUnion[palabraMayorUnion]
        dictWordsMostRepeatUnion[palabraMayorUnion] = 0
        mayorUnion = 0
        thirtyCountUnion -= 1


    palabrasUnion = []
    cantidadUnion = []
    for i in dictWordsMostRepeatUnionFinal:
        palabrasUnion.append(i)
        cantidadUnion.append(dictWordsMostRepeatUnionFinal[i])

    # GRAFICA
    fig, ax = plt.subplots()
    ax.barh(palabrasUnion, cantidadUnion)
    plt.show()



def interseccion(mapaConjuntos: dict):
    dictInterseccion = {}

    cantidad = 0
    for x in mapaConjuntos["Conjunto1"]:
        for y in mapaConjuntos["Conjunto2"]:
            if x == y:
                dictInterseccion[x] = mapaConjuntos["Conjunto1"][x] +  mapaConjuntos["Conjunto2"][y]
                mapaConjuntos["Conjunto1"][x] = 0
                mapaConjuntos["Conjunto2"][y] = 0
                cantidad += 1
                break
        if(cantidad == 30):
            break
            

    thirtyCountInterseccion = len(dictInterseccion)
    mayorInterseccion = 0
    palabraMayorInterseccion = ""
    dictWordsMostRepeatInterseccion = {}

    while thirtyCountInterseccion > 0:
        for z in dictInterseccion:
            if(dictInterseccion[z] > mayorInterseccion):
                    mayorInterseccion = dictInterseccion[z]
                    palabraMayorInterseccion = z
        
        dictWordsMostRepeatInterseccion[palabraMayorInterseccion] = dictInterseccion[palabraMayorInterseccion]
        dictInterseccion[palabraMayorInterseccion] = 0

        mayorInterseccion = 0
        thirtyCountInterseccion -= 1

    palabrasInterseccion = []
    cantidadInterseccion = []

    for i in dictWordsMostRepeatInterseccion:
        palabrasInterseccion.append(i)
        cantidadInterseccion.append(dictWordsMostRepeatInterseccion[i])

    # GRAFICA
    fig, ax = plt.subplots()
    ax.barh(palabrasInterseccion, cantidadInterseccion)
    plt.show()


def graficoNumeroDeTweetsPorFecha():
    conjunto5 = tweepy.Cursor(api.user_timeline, screen_name="UltimaHoracom", tweet_mode="extended").items(200)

    fecha = ""
    cantidad = 1
    dictFechas = {}

    for i in conjunto5:
        if i.created_at.strftime("%Y-%m-%d") != fecha:
            fecha = i.created_at.strftime("%Y-%m-%d")
            cantidad = 1

        dictFechas[fecha] = cantidad
        cantidad += 1

    fechasList = []
    cantidadList = []

    for i in dictFechas:
        fechasList.append(i)
        cantidadList.append(dictFechas[i])

    # GRAFICA
    fig, ax = plt.subplots()
    ax.barh(fechasList, cantidadList)
    plt.show()
    


def graficoNumeroDePersonasPorFecha():
    today = datetime.datetime.today().strftime("%Y-%m-%d")
    last_week = (datetime.datetime.today() - datetime.timedelta(days=7)).strftime("%Y-%m-%d")
    conjunto6 = tweepy.Cursor(api.search_tweets, q="pelota cuadrada"+" since:"+ last_week +" until:"+ today).items(1000)

    name = ""
    fecha = ""
    mismoUsuario = False
    dictFechasNombres = {}
    listOfDifferentUsers = []
    cantidad = 1

    for i in conjunto6:
        mismoUsuario = False
        if i.created_at.strftime("%Y-%m-%d") != fecha:
            fecha = i.created_at.strftime("%Y-%m-%d")
            listOfDifferentUsers = []
            cantidad = 1
        
        for x in listOfDifferentUsers:
            if x == i.user.screen_name:
                mismoUsuario = True
                break
        
        if mismoUsuario == False:
            listOfDifferentUsers.append(i.user.screen_name)
            dictFechasNombres[fecha] = cantidad
            cantidad += 1
          

    fechasList = []
    usuariosDifList = []

    for i in dictFechasNombres:
        fechasList.append(i)
        usuariosDifList.append(dictFechasNombres[i])

    # GRAFICA
    fig, ax = plt.subplots()
    ax.barh(fechasList, usuariosDifList)
    plt.show()


def graficoNumeroDeTweetsPorUsuario():
    today = datetime.datetime.today().strftime("%Y-%m-%d")
    last_week = (datetime.datetime.today() - datetime.timedelta(days=7)).strftime("%Y-%m-%d")
    conjunto7 = tweepy.Cursor(api.search_tweets, q="abc color"+" since:"+ last_week +" until:"+ today).items(500)

    dictUsuarios = {}
    existe = False

    for i in conjunto7:
        existe = False
        for j in dictUsuarios:
            if i.user.screen_name == j:
                existe = True
                break
        
        if existe == False:
            dictUsuarios[i.user.screen_name] = 1
        else:
            dictUsuarios[i.user.screen_name] += 1

    userlist = []
    cantTweetsList = []

    for i in dictUsuarios:
        userlist.append(i)
        cantTweetsList.append(dictUsuarios[i])

    # GRAFICA
    fig, ax = plt.subplots()
    ax.barh(userlist, cantTweetsList)
    plt.show()

def graficoPalabrasDistintasEnConjuntos(mapaConjuntos: dict):
    dictAntiInterseccion = {}

    for x in mapaConjuntos["Conjunto1"]:
        for y in mapaConjuntos["Conjunto2"]:
            if x == y:
                dictAntiInterseccion[x] = mapaConjuntos["Conjunto1"][x] +  mapaConjuntos["Conjunto2"][y]
                mapaConjuntos["Conjunto1"][x] = 0
                mapaConjuntos["Conjunto2"][y] = 0
                break


    wordsDifferentBetweenGroupsList = []
    for x in mapaConjuntos["Conjunto1"]:
        if mapaConjuntos["Conjunto1"][x] != 0:
            wordsDifferentBetweenGroupsList.append(x)

    for y in mapaConjuntos["Conjunto2"]:
        if mapaConjuntos["Conjunto2"][y] != 0:
            wordsDifferentBetweenGroupsList.append(y)

    print("Las Palabras que difieren de un conjunto y otro son: ")
    for i in wordsDifferentBetweenGroupsList:
        print(i)


mapaConjuntos ={}

while option != "0":
    print("----------MENU-----------")
    print("|1:    Conjunto 1       |")
    print("-------------------------")
    print("|2:    Conjunto 2       |")
    print("-------------------------")
    if boolOp1 and boolOp2:
        print("|3:       Union         |")
        print("-------------------------")
        print("|4:   Interseccion      |")
        print("-------------------------")
    print("|5: Grafico por fecha   |")
    print("-------------------------")
    print("|6: Graf. n. de personas|")
    print("-------------------------")  
    print("|7:  Graf. n. de tweets |")
    print("-------------------------")
    if boolOp1 and boolOp2:
        print("|8: Graf. palabras dif. |")
        print("-------------------------")  
    print("|0:      Salir          |")
    print("-------------------------")

    option = input("Seleccione la opcion: ")

    if (option == "1"):
        boolOp1 = True
        mapaConjuntos["Conjunto1"] = conjunto1()
    elif option == "2":
        boolOp2 = True
        mapaConjuntos["Conjunto2"] = conjunto2()
    elif option == "3" and boolOp1 and boolOp2:
        union(mapaConjuntos)
    elif option == "4" and boolOp1 and boolOp2:
        interseccion(mapaConjuntos)
    elif option == "5":
        graficoNumeroDeTweetsPorFecha()
    elif option == "6":
        graficoNumeroDePersonasPorFecha()
    elif option == "7":
        graficoNumeroDeTweetsPorUsuario()
    elif option == "8" and boolOp1 and boolOp2:
        graficoPalabrasDistintasEnConjuntos(mapaConjuntos)
    elif option == "0":
        break
    else:
        print("Opcion incorrecta. Vuelva a intentar")
